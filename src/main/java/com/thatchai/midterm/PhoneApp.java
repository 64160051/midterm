package com.thatchai.midterm;

import java.util.Scanner;

public class PhoneApp {
    static Phone phone1 = new Phone("iPhone 8", 5000);
    static Phone phone2 = new Phone("iPhone 11", 20000);
    static Phone phone3 = new Phone("iPhone 13", 35000);

    public static int input(){
        String strChoice;
        int choice;
        System.out.println("1.iPhone 8  : 8900  Bath");
        System.out.println("2.iPhone 11 : 20000 Bath");
        System.out.println("3.iPhone 12 : 35000 Bath");
        System.out.println("-------------------------");
        System.out.print("Input Phone : ");
        Scanner sc = new Scanner(System.in);
        strChoice = sc.next();
        choice = Integer.parseInt(strChoice);
        return choice;
    }
    public static int inputnumber() {
        String strCount;
        int count;
        System.out.println("-------------------------");
        System.out.print("Input Number : ");
        Scanner sc = new Scanner(System.in);
        strCount = sc.next();
        count = Integer.parseInt(strCount);
        return count;
    }
    
    public static void swith(int choice){
        int count;
        switch(choice){
            case 1:
                phone1.printPrice();
                count = inputnumber();
                phone1.calPrice(count);
                break;
            case 2:
                phone2.printPrice();
                count = inputnumber();
                phone2.calPrice(count);
                break;
            case 3:
                phone3.printPrice();
                count = inputnumber();
                phone3.calPrice(count);
                break;
            case 0:
                System.out.println("Error! Please input number 1-3");
                System.exit(0);
        }
    }
    public static void main(String[] args) {
        int choice = input();
        swith(choice);
    }
}
