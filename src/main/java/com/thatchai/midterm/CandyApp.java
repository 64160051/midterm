package com.thatchai.midterm;

import java.util.Scanner;

public class CandyApp {
    static Candy candy1 = new Candy("Biscuit", 10);
    static Candy candy2 = new Candy("Donut", 15);
    static Candy candy3 = new Candy("Bread", 20);

    public static int input(){
        String strChoice;
        int choice;
        System.out.println("1.Biscuit  :  10 minute");
        System.out.println("2.Donut    :  15 minute");
        System.out.println("3.Bread    :  20 minute");
        System.out.println("-----------------------");
        System.out.print("Input Menu : ");
        Scanner sc = new Scanner(System.in);
        strChoice = sc.next();
        choice = Integer.parseInt(strChoice);
        return choice;
    }
    public static int inputnumber(){
        String strCount;
        int count;
        System.out.println("-----------------------");
        System.out.print("Input Number : ");
        Scanner sc = new Scanner(System.in);
        strCount = sc.next();
        count = Integer.parseInt(strCount);
        return count;
    }
    public static void swith(int choice){
        int count;
        switch(choice){
            case 1:
                candy1.printTime();
                count = inputnumber();
                candy1.calTime(count);
                break;
            case 2:
                candy2.printTime();
                count = inputnumber();
                candy2.calTime(count);
                break;
            case 3:
                candy3.printTime();
                count = inputnumber();
                candy3.calTime(count);
                break;
            case 0:
                System.out.println("Error! Please input menu 1-3");
                System.exit(0);
        }
    }
    public static void main(String[] args) {
        int choice = input();
        swith(choice);
    }
}
