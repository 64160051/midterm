package com.thatchai.midterm;

import java.lang.management.ThreadInfo;

public class Candy {
    private int time;
    private int count;
    private String menu;

    public Candy(String menu,int time){
        this.menu = menu;
        this.time = time;
    }
    public void calTime(int count){
        System.out.println("-----------------------");
        System.out.println("Total Time = "+time*count);
    }
    public void printTime(){
        System.out.println(menu + " Time : "+time);
    }
}

